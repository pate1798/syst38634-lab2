package time;

import static org.junit.Assert.*;

/**
 * @author vishwa patel
 **/
import org.junit.Test;

public class timeTest {

	@Test
	public void testgetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test (expected= NumberFormatException.class)
	public void testGetTotalSecondException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0A");
		fail("The time provided is not valid");
	}
	
	@Test 
	public void testGetTotalSecondBoundryIn() {
		int totalSeconds = Time.getTotalSeconds("00:00:00");
		assertTrue("The time provided does not match the result", totalSeconds == 0);
	}
	
	@Test (expected= NumberFormatException.class)
	public void testGetTotalSecondBoundryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		fail("The time provided is not valid");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//------------------------------------------------------//
	@Test
	public void testgetTotalMilliSecondsRegular() {
		int totalMililiSeconds = Time.getTotalMilliSeconds("12:05:50:05");
		assertTrue("Invalid number of milliseconds", totalMililiSeconds == 5);
//		fail("Invalid number of milliseconds");
	}
	
	@Test (expected=NumberFormatException.class)
	public void testgetTotalMilliSecondsException() {
		int totalMililiSeconds = Time.getTotalMilliSeconds("12:05:50:0A");
//		fail("Invalid number of milliseconds");
	}
	
	@Test
	public void testgetTotalMilliSecondsBoundryIn() {
		int totalMililiSeconds = Time.getTotalMilliSeconds("00:00:00:999");
		assertTrue("Invalid number of milliseconds", totalMililiSeconds == 999);
//		fail("Invalid number of milliseconds");
	}
	
	
	
	
	@Test (expected=NumberFormatException.class)
	public void testgetTotalMilliSecondsBoundryOut() {
		int totalMililiSeconds = Time.getTotalMilliSeconds("12:05:50:1000");
//		fail("Invalid number of milliseconds");
	}
	
}


























